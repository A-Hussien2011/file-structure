package eg.edu.alexu.csd.filestructure.sort.Implementation;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.INode;
import eg.edu.alexu.csd.filestructure.sort.ISort;

import java.util.ArrayList;

/**
 * Created by Bassam on 2/26/2018.
 */
public class Sorter<T extends Comparable<T>> implements ISort<T> {

    @Override
    public IHeap<T> heapSort(final ArrayList<T> unordered) {
        IHeap heap = new Heap();
        heap.build(unordered);
        int size = heap.size();
        INode<T>[] arr = ((Heap) heap).getHeap();
        for (int i = 0; i < unordered.size(); i++) {
            ((Heap)heap).getHeap()[size-i-1].setValue(heap.extract());
        }
        for(int i = 0; i < ((Heap)heap).getHeap().length; i++){
            if(2 * i + 1 <= size) ((Node) ((Heap)heap).getHeap()[i]).setLeftChild(arr[2 * i + 1]);
            if(2 * i + 2 <= size) ((Node) ((Heap)heap).getHeap()[i]).setRightChild(arr[2 * i + 2]);
        }
        return heap;
    }

    /**
     * Insertion Sort
     * take first element as initial point
     * loop 3al elements
     * law element as8ar men i-1
     * loop le7ad ma2af
     * then continue from next point
     */
    @Override
    public void sortSlow(final ArrayList<T> unordered) {

        for (int i = 1; i < unordered.size(); i++) {
//          if arr[i] < arr[i-1] return -1
            int compare = unordered.get(i).compareTo(unordered.get(i - 1));
            if (compare == -1) {
                int j;
                for (j = i - 1; j > 0; j--) {
                    compare = unordered.get(j - 1).compareTo(unordered.get(i));
                    if (compare == -1) {
                        break;
                    }
                }
                unordered.add(j, unordered.remove(i));
            }

        }
    }

    @Override
    public void sortFast(final ArrayList<T> unordered) {

        ArrayList<T> tempArray = (ArrayList<T>) unordered.clone();

        mergeSort(unordered, tempArray, 0, unordered.size() - 1);
    }

    private void mergeSort(ArrayList<T> unorderedArray, ArrayList<T> tempArray, int startIndex, int endIndex) {

        if (startIndex >= endIndex) {
            return;
        }

        int middleIndex = (startIndex + endIndex) / 2;

        mergeSort(unorderedArray, tempArray, startIndex, middleIndex);
        mergeSort(unorderedArray, tempArray, middleIndex + 1, endIndex);
        mergeHalves(unorderedArray, tempArray, startIndex, endIndex);
    }

    private void mergeHalves(ArrayList<T> unorderedArray, ArrayList<T> tempArray, int startIndex, int endIndex) {

        int middleIndex = (startIndex + endIndex) / 2;
        int leftHalfIterator = startIndex;
        int rightHalfIterator = middleIndex + 1;
        int tempArrayIndex = startIndex;

        while (leftHalfIterator <= middleIndex && rightHalfIterator <= endIndex) {
            if (unorderedArray.get(leftHalfIterator).compareTo(unorderedArray.get(rightHalfIterator)) <= 0) {
                tempArray.set(tempArrayIndex, unorderedArray.get(leftHalfIterator));
                leftHalfIterator++;
            } else {
                tempArray.set(tempArrayIndex, unorderedArray.get(rightHalfIterator));
                rightHalfIterator++;
            }
            tempArrayIndex++;
        }

        copyToArrayList(unorderedArray, tempArray, tempArrayIndex, leftHalfIterator, middleIndex);
        copyToArrayList(unorderedArray, tempArray, tempArrayIndex, rightHalfIterator, endIndex);

        copyToArrayList(tempArray, unorderedArray, startIndex, startIndex, endIndex);
    }

    private void copyToArrayList(ArrayList<T> origin, ArrayList<T> destination,
                                 int destinationStartIndex,
                                 int originStartIndex, int originEndIndex) {

        if (originStartIndex > originEndIndex) {
            return;
        }
        if (originStartIndex >= origin.size() || destinationStartIndex >= destination.size()) {
            return;
        }

        int numberOfElementsToCopy = originEndIndex - originStartIndex;
        int originIndex = originStartIndex;

        for (int i = destinationStartIndex; i < destinationStartIndex + numberOfElementsToCopy + 1; i++) {
            destination.set(i, origin.get(originIndex++));
        }
    }
}
