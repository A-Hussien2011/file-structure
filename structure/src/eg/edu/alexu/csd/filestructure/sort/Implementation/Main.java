/**
 * 
 */
package eg.edu.alexu.csd.filestructure.sort.Implementation;

import java.util.ArrayList;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.ISort;

/**
 * @author a7med
 *
 */
public class Main {
	public static void main(String[] args) {

            ArrayList arr = new ArrayList();
            arr.add(1);
            arr.add(2);
            arr.add(8);
            arr.add(7);
            arr.add(3);
            arr.add(10);

            ISort sort = new Sorter();
            IHeap heap = sort.heapSort (arr);
            System.out.println(heap.getRoot().getValue());
            System.out.println(heap.getRoot().getLeftChild().getValue());
            System.out.println(heap.getRoot().getRightChild().getValue());
            System.out.println(heap.getRoot().getLeftChild().getLeftChild().getValue());
            System.out.println(heap.getRoot().getLeftChild().getRightChild().getValue());
            System.out.println(heap.getRoot().getRightChild().getLeftChild().getValue());
//            System.out.println(heap.getRoot().getRightChild().getRightChild().getValue());


    }
}
