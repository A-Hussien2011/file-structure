package eg.edu.alexu.csd.filestructure.sort.Implementation;

import eg.edu.alexu.csd.filestructure.sort.IHeap;
import eg.edu.alexu.csd.filestructure.sort.INode;

import java.util.Collection;

/**
 * Created by Bassam on 2/26/2018.
 */
public class Heap<T extends Comparable<T>> implements IHeap<T> {

	private INode[] heap;
	private int size;

	public Heap(){
		size = -1;
		heap = new Node[7000];
	}
	public Heap(INode[] x){
		size = x.length;
		heap = x;
	}

    @Override
    public INode<T> getRoot() {
        if(heap[0] == null) return null;
        else return (INode<T>) heap[0];
    }

    @Override
    public int size() {
        return size + 1;
    }

    @Override
    public void heapify(INode<T> node) {
    	if(node == null)  throw new RuntimeException();
    	INode parent = node;
    	INode left = parent.getLeftChild();
    	INode right = parent.getRightChild();
    	INode largest = parent;
    	if(left != null && largest.getValue().compareTo(left.getValue()) == -1){
    		largest = left;
    	}
    	if(right != null && largest.getValue().compareTo(right.getValue()) == -1){
    		largest = right;
    	}
    	if(largest != parent){
    		swap(largest, parent);
    		heapify(largest);
    	}
    }

    @Override
    public T extract() {
        T max = (T) heap[0].getValue();
        swap(heap[0], heap[size]);
//        heap[size] = null;
        if(size == 0) return max;
        if(heap[(size-1)/2].getRightChild() != null && heap[(size-1)/2].getRightChild().getValue() == max){
        	((Node)heap[(size-1)/2]).setRightChild(null);
        }else{
        	((Node)heap[(size-1)/2]).setLeftChild(null);
        }
        size --;
        heapify(getRoot());
        return max;

    }

    @Override
    public void insert(T element) {
		size ++;
		if (size == 0) {
			heap[size] = new Node(null, null, null, element);
			return;
		}
		heap[size] = new Node(null, null, heap[(size-1)/2], element);
    	int current = size;
    	while((heap[current].getParent() != null)&&(heap[current].getParent().getValue().compareTo(heap[current].getValue()) < 0 )){
    		INode parent = heap[current].getParent();
    		swap(parent, heap[current]);
    		current = (current-1)/2;
    	}

	}

	@Override
    public void build(Collection<T> unordered) {
		int count = 0;
		while(heap[count] != null){
			heap[count] = null;
			count ++;
		}
		size = -1;
    	Object[] heapValues = unordered.toArray();
    	for(int i = 0; i < heapValues.length; i++){
    		insert((T) heapValues[i]);
    	}
    	for(int i = 0; i < heap.length; i++){
    		if(2 * i + 1 <= size) ((Node) heap[i]).setLeftChild(heap[2 * i + 1]);
    		if(2 * i + 2 <= size) ((Node) heap[i]).setRightChild(heap[2 * i + 2]);
    	}
    }

    public void swap(INode x, INode y){
    	T tmp = (T) x.getValue();
    	x.setValue(y.getValue());
    	y.setValue(tmp);
    }
//    not used unless we need the sorted array
    public INode[] getHeap(){
    	return heap;
    }

}
